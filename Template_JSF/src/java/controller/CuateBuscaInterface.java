package controller;

import java.util.Collection;

/**
 *PRUEBA 2 CuateBuscaInterface.java de JORGE
 */
public interface CuateBuscaInterface {

    public Cuate BuscaCuate(String login, String pwd);

    public Collection<Cuate> ListaCuates();
}
