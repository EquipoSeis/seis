/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 * PRUEBA 3 MapeaCuates.java de JORGE
 * inyeccion de dependencia crea nuevos espacions y libera recursos
 
 */

@ManagedBean(name="buscaCuate", eager=true)
@ApplicationScoped
public class MapeaCuates implements CuateBuscaInterface{
    private Map<String,Cuate> cuates;
    private Collection <Cuate> lista=new ArrayList<Cuate>();
    public MapeaCuates() {
        cuates=new HashMap<>();
        
        addCuate(new Cuate ("patito", "oscar123", "Lopez Avila", "Juan", 2345.60));
        addCuate(new Cuate ("zodiaco", "123456", "Ortega Viera", "Alma", 35.00));
        addCuate(new Cuate ("limbo", "muderico", "muriño Perez", "Pedro",200.00));
        addCuate(new Cuate("maveric","54321","Huerta Moreno","Antonio",200.00));
    }
    
    public Cuate BuscaCuate(String login, String pwd)
    {
        Cuate cuate=null;
        if (login !=null);
        {
            cuate=cuates.get(login.toLowerCase());
            if (cuate !=null){
                if (cuate.getPwd().equals(pwd)){
                    
                    return cuate;
                }
                else
                    return (null);
            }
        }
        return cuate;
    }
    
    private void addCuate(Cuate cuate)
    {
        cuates.put(cuate.getUser(), cuate);
        lista.add(cuate);
        cuates.put(cuate.getUser(),cuate);
    }

    @Override
    public Collection<Cuate> ListaCuates() {

        return lista;
    }
    
}
